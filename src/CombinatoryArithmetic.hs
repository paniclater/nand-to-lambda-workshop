module CombinatoryArithmetic where

import CombinatoryBoolean
import Voltage

halfAdder :: Voltage -> Voltage -> (Voltage, Voltage)
halfAdder = undefined

fullAdder :: Voltage -> Voltage -> Voltage -> (Voltage, Voltage)
fullAdder = undefined

add16 :: Voltage16 -> Voltage16 -> Voltage16
add16 = undefined

inc16 :: Voltage16 -> Voltage16
inc16 = undefined