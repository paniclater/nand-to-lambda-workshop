module ALU where

import CombinatoryBoolean
import CombinatoryArithmetic
import Voltage

-- if (zx == 1) set x = 0        // 16-bit constant
-- if (nx == 1) set x = !x       // bitwise not
-- if (zy == 1) set y = 0        // 16-bit constant
-- if (ny == 1) set y = !y       // bitwise not
-- if (f == 1)  set out = x + y  // integer 2's complement addition
-- if (f == 0)  set out = x & y  // bitwise and
-- if (no == 1) set out = !out   // bitwise not
-- if (out == 0) set zr = 1
-- if (out < 0) set ng = 1

alu :: Voltage16 -> Voltage16 -> (Voltage, Voltage, Voltage, Voltage, Voltage, Voltage) -> Voltage
alu = undefined

alu' :: Voltage16 -> Voltage16 -> (Voltage, Voltage, Voltage, Voltage, Voltage, Voltage) -> (Voltage16, Voltage, Voltage)
alu' = undefined

addControlBits = (Low, Low, Low, Low, High, Low)
subtractControlBits = (Low, High, Low, Low, High, High)
addNegateControlBits = (Low, Low, Low, Low, High, High)

(o, ng, zr) = alu' one16 one16 addControlBits
(o1, ng1, zr1) = alu' two16 one16 subtractControlBits
(o2, ng2, zr2) = alu' low16 low16 addNegateControlBits
(o3, ng3, zr3) = alu' low16 two16 subtractControlBits
(o4, ng4, zr4) = alu' one16 one16 subtractControlBits

onePlusOneIsTwo = (o, ng, zr) == (two16, Low, Low)
twoMinusOneIsTwo = (o1, ng1, zr1) == (one16, Low, Low)
negationOfZero = (o2, ng2, zr2) == (high16, High, Low)
zeroMinusOneIsNegativeOne = (o3, ng3, zr3) == (negativeTwo16, High, Low)
zeroIsZero = (o4, ng4, zr4) == (low16, Low, High)

testPass = onePlusOneIsTwo && twoMinusOneIsTwo && negationOfZero && zeroMinusOneIsNegativeOne && zeroIsZero