module Validator where

import Control.Applicative (Alternative, empty, (<|>))
import Data.Monoid ((<>))

data Validator a  b = Warning a | Report b deriving Show
data NumberError = TooBig | TooSmall deriving Show

instance Functor (Validator a) where
  fmap = undefined
  fmap = undefined

instance Monoid a => Applicative (Validator a) where
  pure = Report
  (<*>) = undefined

instance Monoid a => Alternative (Validator a) where
  empty = Warning mempty
  (<|>) = undefined

-- warnings

validateNumber :: Int -> Validator[NumberError] Int
validateNumber y
  | y < 50 = Warning [TooSmall]
  | y > 100 = Warning [TooBig]
  | otherwise = Report y





