module CombinatoryBoolean where

import Voltage

-- Single Bit Operations

nand :: Voltage -> Voltage -> Voltage
nand = undefined

not' :: Voltage -> Voltage
not' = undefined

and' :: Voltage -> Voltage -> Voltage
and' = undefined

or' :: Voltage -> Voltage -> Voltage
or' = undefined

xor :: Voltage -> Voltage -> Voltage
xor = undefined

mux :: Voltage -> Voltage -> Voltage -> Voltage
mux = undefined

dmux :: Voltage -> Voltage -> (Voltage, Voltage)
dmux = undefined

-- 16 Bit Operations

not16 :: Voltage16 -> Voltage16
not16 = undefined

and16 :: Voltage16 -> Voltage16 -> Voltage16
and16 = undefined

or16 :: Voltage16 -> Voltage16 -> Voltage16
or16 = undefined

mux16 :: Voltage16 -> Voltage16 -> Voltage -> Voltage16
mux16 = undefined

-- multiway operations

dmux4Way :: Voltage -> (Voltage, Voltage) -> (Voltage, Voltage, Voltage, Voltage)
dmux4Way = undefined

or8Way :: (Voltage, Voltage, Voltage, Voltage, Voltage, Voltage, Voltage, Voltage) -> Voltage
or8Way = undefined

dmux8Way :: Voltage -> (Voltage, Voltage, Voltage)  -> (Voltage, Voltage, Voltage, Voltage, Voltage, Voltage, Voltage, Voltage)
dmux8Way = undefined

mux4Way16 :: (Voltage16, Voltage16, Voltage16, Voltage16) -> (Voltage, Voltage) -> Voltage16
mux4Way16 = undefined
mux8Way16 :: (Voltage16, Voltage16, Voltage16, Voltage16, Voltage16, Voltage16, Voltage16, Voltage16) -> (Voltage, Voltage, Voltage) -> Voltage16
mux8Way16 = undefined