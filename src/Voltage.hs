module Voltage where

  data Voltage = High | Low deriving (Show, Eq)

  data Voltage16 = Voltage16
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage
    Voltage deriving (Show, Eq)

  low16 :: Voltage16
  low16 = Voltage16 Low Low Low Low Low Low Low Low Low Low Low Low Low Low Low Low
  high16 :: Voltage16
  high16 = Voltage16 High High High High High High High High High High High High High High High High
  one16 :: Voltage16
  one16 = Voltage16 High Low Low Low Low Low Low Low Low Low Low Low Low Low Low Low
  two16 :: Voltage16
  two16 = Voltage16 Low High Low Low Low Low Low Low Low Low Low Low Low Low Low Low
  negativeTwo16 :: Voltage16
  negativeTwo16 = Voltage16 Low High High High High High High High High High High High High High High High
